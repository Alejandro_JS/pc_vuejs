/*
Nombre de aplicación	platzimusic
Clave API	bce2e9e53cd3c65ae9e4759dc9b4dd86
Secreto compartido	7a7c7d11fff57bd877f4e1920b348cde
Registrado para	masterXCX
*/
export default{
    appName: 'platzimusic',
    apiKey: 'bce2e9e53cd3c65ae9e4759dc9b4dd86',
    secret: '7a7c7d11fff57bd877f4e1920b348cde',
    registeredTo: 'masterXCX'
}