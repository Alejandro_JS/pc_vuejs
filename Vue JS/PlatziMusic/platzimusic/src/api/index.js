import config from './config'
const { apiKey } = config
const URL = `https://ws.audioscrobbler.com/2.0/?method=geo.gettopartists&country=:country&api_key=${apiKey}&format=json`
console.log(URL)

export default function getArtistd(country){
    const urlx = URL.replace(':country', country)
    return fetch(urlx)
        .then(res => res.json())
        .then(json => json.topartists.artist)
}